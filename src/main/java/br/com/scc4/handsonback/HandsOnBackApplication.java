package br.com.scc4.handsonback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HandsOnBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(HandsOnBackApplication.class, args);
	}

}
