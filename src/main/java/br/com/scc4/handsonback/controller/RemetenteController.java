package br.com.scc4.handsonback.controller;

import br.com.scc4.handsonback.dto.CriarRemetenteDTO;
import br.com.scc4.handsonback.dto.RemetenteDTO;
import br.com.scc4.handsonback.service.RemetenteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("remetentes")
public class RemetenteController {

    @Autowired
    private RemetenteService service;

    @GetMapping
    public List<RemetenteDTO> listarRemetentes() {
        return service.listarRemetentes();
    }

    @GetMapping("{remetenteId}")
    public RemetenteDTO remetentePorId(@PathVariable Integer remetenteId) {
        return service.remetentePorId(remetenteId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RemetenteDTO incluirRemetente(@RequestBody @Valid CriarRemetenteDTO dto) {
        return service.incluirRemetente(dto);
    }

    @PutMapping("{remetenteId}")
    public RemetenteDTO editarRemetente(@PathVariable Integer remetenteId,
                                        @RequestBody CriarRemetenteDTO dto) {
        return service.editarRemetente(remetenteId, dto);
    }

    @DeleteMapping("{remetenteId}")
    public void excluirRemetente(@PathVariable Integer remetenteId) {
        service.excluirRemetente(remetenteId);
    }

}
