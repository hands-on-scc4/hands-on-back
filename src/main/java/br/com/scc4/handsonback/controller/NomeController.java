package br.com.scc4.hands_on.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NomeController {

    @GetMapping("nomeBibliografico")
    @ResponseStatus(HttpStatus.OK)
    public String nomeBibliografico (@RequestParam String nome) {

        String ultimoNome = nome.substring( (nome.lastIndexOf(" ")+1));
        String primeiroNome = nome.replaceAll(ultimoNome, "");
        String nomeBibliografico = ultimoNome.toUpperCase() + ", " + primeiroNome;

        return nomeBibliografico;

    }

}
