package br.com.scc4.hands_on.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
public class NumeroController {

    @GetMapping("listaReversa")
    @ResponseStatus(HttpStatus.OK)
    public List<Integer> listaReversa(@RequestParam List<Integer> lista) {

        Collections.reverse(lista);
        return lista;

    }

    @GetMapping("imprimirImpares")
    @ResponseStatus(HttpStatus.OK)
    public List<Integer> imprimirImpares(@RequestParam List<Integer> lista) {

        List<Integer> impares = new ArrayList<>();

        for (Integer numero: lista) {
            if (numero % 2 == 1) {
                impares.add(numero);
            }
        }

        Collections.reverse(impares);
        return impares;

    }

    @GetMapping("imprimirPares")
    @ResponseStatus(HttpStatus.OK)
    public List<Integer> imprimirPares(@RequestParam List<Integer> lista) {

        List<Integer> pares = new ArrayList<>();

        for (Integer numero: lista) {
            if (numero % 2 == 0) {
                pares.add(numero);
            }
        }

        Collections.reverse(pares);
        return pares;

    }

}
