package br.com.scc4.hands_on.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PalavraController {

    @GetMapping("tamanho")
    @ResponseStatus(HttpStatus.OK)
    public Integer tamanho (@RequestParam String palavra) {

        return palavra.length();

    }

    @GetMapping("maisculas")
    @ResponseStatus(HttpStatus.OK)
    public String maisculas (@RequestParam String palavra) {

        return palavra.toUpperCase();

    }

    @GetMapping("vogais")
    @ResponseStatus(HttpStatus.OK)
    public String vogais (@RequestParam String palavra) {

        char[] vogais = {'a', 'e', 'i', 'o', 'u'};
        String vogaisPalavra = "";

        for (int i = 0; i < palavra.length(); i++) {
            for (int j = 0; j < vogais.length; j++) {
                if (Character.toLowerCase(palavra.charAt(i)) == vogais[j]) {
                    vogaisPalavra += palavra.charAt(i);
                }
            }
        }

        return vogaisPalavra;

    }

    @GetMapping("consoantes")
    @ResponseStatus(HttpStatus.OK)
    public String consoantes (@RequestParam String palavra) {

        char[] consoantes = {'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'};
        String consoantesPalavra = "";

        for (int i = 0; i < palavra.length(); i++) {
            for (int j = 0; j < consoantes.length; j++) {
                if (Character.toLowerCase(palavra.charAt(i)) == consoantes[j]) {
                    consoantesPalavra += palavra.charAt(i);
                }
            }
        }

        return consoantesPalavra;

    }

}
