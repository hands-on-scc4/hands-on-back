package br.com.scc4.handsonback.controller;

import br.com.scc4.handsonback.dto.CriarDestinatarioDTO;
import br.com.scc4.handsonback.dto.DestinatarioDTO;
import br.com.scc4.handsonback.service.DestinatarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("destinatarios")
public class DestinatarioController {

    @Autowired
    private DestinatarioService service;

    @GetMapping
    public List<DestinatarioDTO> listarDestinatarios() {
        return service.listarDestinatarios();
    }

    @GetMapping("{destinatarioId}")
    public DestinatarioDTO destinatarioPorId(@PathVariable Integer destinatarioId) {
        return service.destinatarioPorId(destinatarioId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public DestinatarioDTO incluirDestinatario(@RequestBody @Valid CriarDestinatarioDTO dto) {
        return service.incluirDestinatario(dto);
    }

    @PutMapping("{destinatarioId}")
    public DestinatarioDTO editarDestinatario(@PathVariable Integer destinatarioId,
                                              @RequestBody CriarDestinatarioDTO dto) {
        return service.editarDestinatario(destinatarioId, dto);
    }

    @DeleteMapping("{destinatarioId}")
    public void excluirDestinatario(@PathVariable Integer destinatarioId) {
        service.excluirDestinatario(destinatarioId);
    }

}
