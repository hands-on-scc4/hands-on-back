package br.com.scc4.handsonback.repository;

import br.com.scc4.handsonback.entity.Remetente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RemetenteRepository extends JpaRepository<Remetente, Integer> {
}
