package br.com.scc4.handsonback.repository;

import br.com.scc4.handsonback.entity.Destinatario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DestinatarioRepository extends JpaRepository<Destinatario, Integer> {
}
