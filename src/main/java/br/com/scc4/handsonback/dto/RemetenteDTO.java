package br.com.scc4.handsonback.dto;

import br.com.scc4.handsonback.entity.Mensagem;
import br.com.scc4.handsonback.entity.Remetente;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter @Setter @NoArgsConstructor
public class RemetenteDTO {

    private Integer id;
    private String nome;
    private String telefone;
    private String cpf;
    private String endereco;
    private String email;
    private List<String> mensagens;

    public RemetenteDTO(Remetente remetente) {
        this.id = remetente.getId();
        this.nome = remetente.getNome();
        this.telefone = remetente.getTelefone();
        this.cpf = remetente.getCpf();
        this.endereco = remetente.getEndereco();
        this.email = remetente.getEmail();
        this.mensagens = remetente.getMensagens()
                .stream()
                .map(Mensagem::getTitulo)
                .collect(Collectors.toList());
    }

}
