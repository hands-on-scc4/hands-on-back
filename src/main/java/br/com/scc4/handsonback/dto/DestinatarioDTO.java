package br.com.scc4.handsonback.dto;

import br.com.scc4.handsonback.entity.Destinatario;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter @Setter @NoArgsConstructor
public class DestinatarioDTO {

    private Integer id;
    private String nome;
    private String telefone;
    private String cpf;
    private String endereco;
    private String email;
    private String tituloMensagem;

    public DestinatarioDTO(Destinatario destinatario) {
        this.id = destinatario.getId();
        this.nome = destinatario.getNome();
        this.telefone = destinatario.getTelefone();
        this.cpf = destinatario.getCpf();
        this.endereco = destinatario.getEndereco();
        this.email = destinatario.getEmail();
        this.tituloMensagem = destinatario.getMensagem().getTitulo();
    }
}
