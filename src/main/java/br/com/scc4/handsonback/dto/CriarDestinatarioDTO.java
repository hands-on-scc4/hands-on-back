package br.com.scc4.handsonback.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CriarDestinatarioDTO {

    private String nome;
    private String telefone;
    private String cpf;
    private String endereco;
    private String email;

}
