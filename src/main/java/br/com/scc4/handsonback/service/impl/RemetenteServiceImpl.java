package br.com.scc4.handsonback.service.impl;

import br.com.scc4.handsonback.dto.CriarRemetenteDTO;
import br.com.scc4.handsonback.dto.RemetenteDTO;
import br.com.scc4.handsonback.entity.Remetente;
import br.com.scc4.handsonback.repository.RemetenteRepository;
import br.com.scc4.handsonback.service.RemetenteService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RemetenteServiceImpl implements RemetenteService {

    @Autowired
    private RemetenteRepository repository;

    @Override
    public List<RemetenteDTO> listarRemetentes() {
        List<Remetente> remetentes = repository.findAll();

        return remetentes.stream()
                .map(RemetenteDTO::new)
                .collect(Collectors.toList());
    }

    @Override
    public RemetenteDTO remetentePorId(Integer remetenteId) {
        Remetente remetente = buscaRemetentePorId(remetenteId);
        remetente.setNome(remetente.getNome());
        remetente.setTelefone(remetente.getTelefone());
        remetente.setCpf(remetente.getCpf());
        remetente.setEndereco(remetente.getEndereco());
        remetente.setEmail(remetente.getEmail());

        return new RemetenteDTO(remetente);
    }

    @Override
    public RemetenteDTO incluirRemetente(CriarRemetenteDTO dto) {
        Remetente remetente = new Remetente();
        remetente.setNome(dto.getNome());
        remetente.setTelefone(dto.getTelefone());
        remetente.setCpf(dto.getCpf());
        remetente.setEndereco(dto.getEndereco());
        remetente.setEmail(dto.getEmail());

        Remetente salvaRemetente = repository.save(remetente);

        return new RemetenteDTO(salvaRemetente);
    }

    @Override
    public RemetenteDTO editarRemetente(Integer remetenteId, CriarRemetenteDTO dto) {
        Remetente remetente = buscaRemetentePorId(remetenteId);
        BeanUtils.copyProperties(dto, remetente);
        Remetente atualizaRemetente = repository.save(remetente);

        return new RemetenteDTO(atualizaRemetente);
    }

    @Override
    public void excluirRemetente(Integer remetenteId) {
        repository.deleteById(remetenteId);
    }

    private Remetente buscaRemetentePorId(Integer remetenteId) {
        return repository.findById(remetenteId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

}