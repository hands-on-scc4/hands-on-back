package br.com.scc4.handsonback.service;

import br.com.scc4.handsonback.dto.CriarRemetenteDTO;
import br.com.scc4.handsonback.dto.RemetenteDTO;

import java.util.List;

public interface RemetenteService {

    List<RemetenteDTO> listarRemetentes();
    RemetenteDTO remetentePorId(Integer remetenteId);
    RemetenteDTO incluirRemetente(CriarRemetenteDTO dto);
    RemetenteDTO editarRemetente(Integer remetenteId, CriarRemetenteDTO dto);
    void excluirRemetente(Integer remetenteId);
}
