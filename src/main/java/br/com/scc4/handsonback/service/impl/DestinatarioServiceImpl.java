package br.com.scc4.handsonback.service.impl;

import br.com.scc4.handsonback.dto.CriarDestinatarioDTO;
import br.com.scc4.handsonback.dto.DestinatarioDTO;
import br.com.scc4.handsonback.entity.Destinatario;
import br.com.scc4.handsonback.repository.DestinatarioRepository;
import br.com.scc4.handsonback.service.DestinatarioService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DestinatarioServiceImpl implements DestinatarioService {

    @Autowired
    private DestinatarioRepository repository;

    @Override
    public List<DestinatarioDTO> listarDestinatarios() {
        List<Destinatario> destinatarios = repository.findAll();

        return destinatarios.stream()
                .map(DestinatarioDTO::new)
                .collect(Collectors.toList());
    }

    @Override
    public DestinatarioDTO destinatarioPorId(Integer destinatarioId) {
        Destinatario destinatario = new Destinatario();
        destinatario.setNome(destinatario.getNome());
        destinatario.setTelefone(destinatario.getTelefone());
        destinatario.setCpf(destinatario.getCpf());
        destinatario.setEndereco(destinatario.getEndereco());
        destinatario.setEmail(destinatario.getEmail());

        return new DestinatarioDTO(destinatario);
    }

    @Override
    public DestinatarioDTO incluirDestinatario(CriarDestinatarioDTO dto) {
        Destinatario destinatario = new Destinatario();
        destinatario.setNome(dto.getNome());
        destinatario.setTelefone(dto.getTelefone());
        destinatario.setCpf(dto.getCpf());
        destinatario.setEndereco(dto.getEndereco());
        destinatario.setEmail(dto.getEmail());

        Destinatario salvaDestinatario = repository.save(destinatario);

        return new DestinatarioDTO(salvaDestinatario);
    }

    @Override
    public DestinatarioDTO editarDestinatario(Integer destinatarioId, CriarDestinatarioDTO dto) {
        Destinatario destinatario = buscaDestinatarioPorId(destinatarioId);
        BeanUtils.copyProperties(dto, destinatario);
        Destinatario atualizaDestinatario = repository.save(destinatario);

        return new DestinatarioDTO(atualizaDestinatario);
    }

    @Override
    public void excluirDestinatario(Integer destinatarioId) {
        repository.deleteById(destinatarioId);
    }

    private Destinatario buscaDestinatarioPorId(Integer destinatarioId) {
        return repository.findById(destinatarioId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

}
