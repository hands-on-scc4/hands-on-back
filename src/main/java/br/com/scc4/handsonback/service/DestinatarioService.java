package br.com.scc4.handsonback.service;

import br.com.scc4.handsonback.dto.CriarDestinatarioDTO;
import br.com.scc4.handsonback.dto.DestinatarioDTO;

import java.util.List;

public interface DestinatarioService {

    List<DestinatarioDTO> listarDestinatarios();
    DestinatarioDTO destinatarioPorId(Integer destinatarioId);
    DestinatarioDTO incluirDestinatario(CriarDestinatarioDTO dto);
    DestinatarioDTO editarDestinatario(Integer destinatarioId, CriarDestinatarioDTO dto);
    void excluirDestinatario(Integer destinatarioId);

}
