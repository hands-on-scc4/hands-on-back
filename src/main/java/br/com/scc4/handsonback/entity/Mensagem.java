package br.com.scc4.handsonback.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "TB_SCC4_MENSAGEM")
public class Mensagem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String titulo;

    @Column
    private String corpo;

    @ManyToOne
    private Remetente remetente;

    @OneToMany
    @JoinColumn(name = "mensagem_id")
    private List<Destinatario> destinatarios = new ArrayList<>();

}
