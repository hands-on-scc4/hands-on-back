package br.com.scc4.handsonback.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@MappedSuperclass
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String nome;

    @Column
    private String telefone;

    @Column
    private String cpf;

    @Column
    private String endereco;

    @Column
    private String email;

}
