package br.com.scc4.handsonback.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "TB_SCC4_DESTINATARIO")
public class Destinatario extends Cliente {

    @ManyToOne
    private Mensagem mensagem;

}
