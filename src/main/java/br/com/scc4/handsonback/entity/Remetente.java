package br.com.scc4.handsonback.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "TB_SCC4_REMETENTE")
public class Remetente extends Cliente {

    @OneToMany
    @JoinColumn(name = "remetente_id")
    private List<Mensagem> mensagens = new ArrayList<>();

}
