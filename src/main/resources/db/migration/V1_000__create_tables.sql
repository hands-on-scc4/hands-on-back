CREATE TABLE TB_SCC4_REMETENTE (

    id              INTEGER AUTO_INCREMENT  PRIMARY KEY,
    nome            VARCHAR(100),
    telefone        VARCHAR(20),
    cpf             VARCHAR(11),
    endereco        VARCHAR(100),
    email           VARCHAR(100)

);

CREATE TABLE TB_SCC4_MENSAGEM (

    id              INTEGER AUTO_INCREMENT  PRIMARY KEY,
    titulo          VARCHAR(30),
    corpo           VARCHAR(500),
    remetente_id    INTEGER,
    FOREIGN KEY (remetente_id) REFERENCES TB_SCC4_REMETENTE (id)

);

CREATE TABLE TB_SCC4_DESTINATARIO (

    id              INTEGER AUTO_INCREMENT  PRIMARY KEY,
    nome            VARCHAR(100),
    telefone        VARCHAR(20),
    cpf             VARCHAR(11),
    endereco        VARCHAR(100),
    email           VARCHAR(100),
    mensagem_id     INTEGER,
    FOREIGN KEY (mensagem_id) REFERENCES TB_SCC4_MENSAGEM (id)

);